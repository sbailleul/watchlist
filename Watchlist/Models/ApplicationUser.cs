using System.Collections;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace Watchlist.Models
{
    public class ApplicationUser: IdentityUser
    {
        public string Firstname { get; set; }
        public ApplicationUser(): base()
        {
        }
        public virtual ICollection<UserMovie> WatchList { get; set; } = new HashSet<UserMovie>();
    }
}